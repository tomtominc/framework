﻿using UnityEngine;

namespace Framework.EditorTools
{
	public class LabelAliasAttribute : PropertyAttribute 
	{
		public string label;
		public LabelAliasAttribute()
		{
			this.label = null;
		}
		public LabelAliasAttribute(string label)
		{
			this.label = label;
		}
	}
}