﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using Framework;

public class EditorColor
{
    public static Color red { get { return HexToColor("FF5252FF"); } }

    public static Color pink { get { return HexToColor("FF8F8FFF"); } }

    public static Color green { get { return HexToColor("48FF00FF"); } }

    public static Color greenyellow { get { return HexToColor("70FF83FF"); } }

    public static Color blue { get { return HexToColor("00F4FFFF"); } }

    public static Color bluelight { get { return HexToColor("BFF2FDFF"); } }

    public static Color orange { get { return HexToColor("F39A69FF"); } }

    public static Color yellow { get { return HexToColor("FFD700FF"); } }

    public static Color greyblue { get { return HexToColor("A5B7BBFF"); } }

    public static Color grey { get { return HexToColor("8C8C8C05"); } }

    public static Color purple { get { return HexToColor("a810f7"); } }

    public static Color bluegreen { get { return HexToColor("10f7a3"); } }

    public static Color yelloworange { get { return HexToColor("ffdf2c"); } }

    public static Color redlight { get { return HexToColor("ff2c5e"); } }



    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, 255);
    }
}

public static class EditorIcons
{
    //    public char this[ string s ]
    //    {
    //        get {  return unicodeMap [s]; }
    //    }

    public static Dictionary < string , char > unicode = new Dictionary<string, char>()
    {
        { "List"                , '\u2630' },
        { "Plus"                , '\u2795' },
        { "One_Circle_Filled"   , '\u278A' },
        { "Diamond_Sliced"      , '\u2756' },
        { "Star_Filled"         , '\u2605' },
        { "Star_Empty"          , '\u2606' },
        { "Key"                 , '★' }
    };
}


public static class EditorGUIExtensions
{
    public static string ToolsPath = "Assets/PotionSoup.Essentials/Tools/";

    static public int DrawSelectableHeader(string aSelectionID, params string[] aText)
    {
        GUILayout.Space(3f);
        GUILayout.BeginHorizontal();
        GUILayout.Space(3f);
		
        GUI.changed = false;

        int selectedHeader = EditorPrefs.GetInt("SelectableHeader_" + aSelectionID, 0);
        for (int i = 0; i < aText.Length; i++)
        {
            string text = "<size=11>" + aText[i].ToString() + "</size>";
            if (i == selectedHeader)
            {
                text = "<b>" + text + "</b>";
            }
            if (!GUILayout.Toggle(true, text, "dragtab", GUILayout.MinWidth(20f)))
            {
                selectedHeader = i;
            }
        }
        EditorPrefs.SetInt("SelectableHeader_" + aSelectionID, selectedHeader);

        GUILayout.Space(2f);
        GUILayout.EndHorizontal();

        return selectedHeader;
    }

    public static int DrawSelectableHeader(string aSelectionID, Color activeColor, params object[] aText)
    {
        GUILayout.Space(3f);
        GUILayout.BeginHorizontal();
        GUILayout.Space(3f);

        GUI.changed = false;

        int selectedHeader = EditorPrefs.GetInt("ToolbarHeader_" + aSelectionID, 0);
        for (int i = 0; i < aText.Length; i++)
        {
            string text = "<size=11>" + aText[i].ToString() + "</size>";
            Color headerColor = i == selectedHeader ? activeColor : Color.white;
            string unicodeIcon = EditorIcons.unicode[i == selectedHeader ? "Star_Filled" : "Star_Empty"].ToString();
            SetBackgroundColor(headerColor);

            if (i == selectedHeader)
            {
                text = "<b>" + text + "</b>";
            }


            if (!GUILayout.Toggle(true, string.Format("{0} {1}", unicodeIcon, text), "dragtab", GUILayout.MinWidth(20f)))
            {
                selectedHeader = i;
            }

            RestoreBackgroundColor();
        }
        EditorPrefs.SetInt("ToolbarHeader_" + aSelectionID, selectedHeader);

        GUILayout.Space(2f);
        GUILayout.EndHorizontal();

        return selectedHeader;
    }

    public static string DrawSearchToolbar(string m_BrowserFilter)
    {
        GUILayout.Space(2f);
        EditorGUIExtensions.SetBackgroundColor(Color.white);
        EditorGUILayout.BeginHorizontal((GUIStyle)"Toolbar");
        m_BrowserFilter = EditorGUILayout.TextField(m_BrowserFilter, new GUIStyle("ToolbarSeachTextField"), GUILayout.ExpandWidth(true));
        if (GUILayout.Button(string.Empty, "ToolbarSeachCancelButton"))
        {
            m_BrowserFilter = string.Empty;
        }
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(2f);
        EditorGUIExtensions.RestoreBackgroundColor();

        return m_BrowserFilter;
    }

    //public static bool IsPressingKey ()

    public static GUIStyle GetFontStyle(string baseStyle, int fontSize, TextAnchor anchor, FontStyle fontStyle)
    {
        GUIStyle _fontStyle = new GUIStyle(baseStyle);
        _fontStyle.richText = true;
        _fontStyle.fontSize = fontSize;
        _fontStyle.alignment = anchor;
        _fontStyle.fontStyle = fontStyle;

        return _fontStyle;
    }

    public static void SetToggleButton(string key, bool isOn)
    {
        EditorPrefs.SetBool("ToggleButtonAuto" + key, isOn);
    }

    public static bool ToggleTitle(string titleName, string key, Color onColor)
    {
        bool toggle = EditorPrefs.GetBool("ToggleButtonAuto" + key, false);

        Color bg = toggle ? onColor : Color.white;

        EditorGUIExtensions.SetBackgroundColor(bg);

        GUILayout.BeginHorizontal("OL Title");

        EditorPrefs.SetBool("ToggleButtonAuto" + key, GUILayout.Toggle(toggle, "", GUILayout.Width(16)));
        GUILayout.Label(titleName);

        GUILayout.EndHorizontal();

        EditorGUIExtensions.RestoreBackgroundColor();

        EditorGUILayout.Space();

        return toggle;
    }

    public static bool ToggleButton(string buttonName, string key, GUIStyle style, Color onColor, params GUILayoutOption[] options)
    {
        bool toggle = EditorPrefs.GetBool("ToggleButtonAuto" + key, false);

        Color bg = toggle ? onColor : Color.white;
		
        EditorGUIExtensions.SetBackgroundColor(bg);
		
        if (GUILayout.Button(buttonName, style, options))
        {
            toggle = !toggle;
            EditorPrefs.SetBool("ToggleButtonAuto" + key, toggle);
        }
		
        EditorGUIExtensions.RestoreBackgroundColor();
		
        return toggle;
    }

    public static void DisableToggleGroup(string[] buttonNames)
    {
        foreach (var buttonName in buttonNames)
        {
            EditorPrefs.SetBool("ToggleButtonGroup" + buttonName, false);
        }

    }

    public static string ToggleButtonGroup(string[] buttonNames, GUIStyle style, Color onColor, params GUILayoutOption[] options)
    {
        string currentButton = string.Empty;

        foreach (var buttonName in buttonNames)
        {
            bool toggle = EditorPrefs.GetBool("ToggleButtonGroup" + buttonName, false);

            Color bg = toggle ? onColor : Color.white;

            if (toggle)
                currentButton = buttonName;

            EditorGUIExtensions.SetBackgroundColor(bg);

            if (GUILayout.Button(buttonName, style, options))
            {
                foreach (var key in buttonNames)
                {
                    EditorPrefs.SetBool("ToggleButtonGroup" + key, false);
                }

                EditorPrefs.SetBool("ToggleButtonGroup" + buttonName, true);

                currentButton = buttonName;
            }

            EditorGUIExtensions.RestoreBackgroundColor();
        }

        return currentButton;
    }



    public static void DisplayProperty(bool display, SerializedProperty property)
    {
        if (display)
        {
            EditorGUILayout.PropertyField(property, true);
        }
    }

    public static string LabelField(string label, string text, GUIStyle style, Color backgroundColor)
    {

        SetBackgroundColor(backgroundColor);

        text = EditorGUILayout.TextField(label, text, style);

        RestoreBackgroundColor();

        return text;
    }

    public static int IntField(object obj, FieldInfo prop)
    {
        var value = prop.GetValue(obj);
        var attributes = prop.GetCustomAttributes(true);

        if (attributes.Length > 0)
        {
            var attribute = attributes[0];

            if (attribute.GetType() == typeof(RangeAttribute))
            {
                var range = (RangeAttribute)attribute;

                prop.SetValue(obj, EditorGUILayout.IntSlider(prop.Name, (int)value, (int)range.min, (int)range.max));
            }
            else
            {
                prop.SetValue(obj, EditorGUILayout.IntField(prop.Name, (int)value));
            }
        }
        else
        { 
            prop.SetValue(obj, EditorGUILayout.IntField(prop.Name, (int)value));

        }

        return (int)prop.GetValue(obj);
    }

    public static float FloatField(object obj, FieldInfo prop)
    {
        var value = prop.GetValue(obj);
        var attributes = prop.GetCustomAttributes(true);

        if (attributes.Length > 0)
        {
            var attribute = attributes[0];

            if (attribute.GetType() == typeof(RangeAttribute))
            {
                var range = (RangeAttribute)attribute;

                prop.SetValue(obj, EditorGUILayout.Slider(prop.Name, (float)value, (float)range.min, (float)range.max));
            }
            else
            {
                prop.SetValue(obj, EditorGUILayout.FloatField(prop.Name, (float)value));
            }
        }
        else
        { 
            prop.SetValue(obj, EditorGUILayout.FloatField(prop.Name, (float)value));

        }

        return (float)prop.GetValue(obj);
    }

    public static void DrawScriptField(SerializedObject serializedObject)
    {
        serializedObject.Update();
        SerializedProperty prop = serializedObject.FindProperty("m_Script");
        EditorGUILayout.PropertyField(prop, true, new GUILayoutOption[0]);
        serializedObject.ApplyModifiedProperties();
    }

    static public bool DrawHeader(string aText, GUILayoutOption option, bool aDefaultValue = true, bool aForceDefault = false)
    {
        if (aForceDefault)
        {
            EditorPrefs.SetBool("DrawHeader_" + aText, aDefaultValue);
        }
        bool clicked = EditorPrefs.GetBool("DrawHeader_" + aText, aDefaultValue);
        GUILayout.Space(3f);
        GUILayout.BeginHorizontal();
        GUILayout.Space(3f);

        string text = "<size=11>" + aText + "</size>";
        if (clicked)
        {
            text = "<b>" + text + "</b>";
        }
        if (!GUILayout.Toggle(true, text, "dragtab", GUILayout.MinWidth(20f), option))
        {
            clicked = !clicked;
            EditorPrefs.SetBool("DrawHeader_" + aText, clicked);
        }
		
        GUILayout.Space(2f);
        GUILayout.EndHorizontal();

        return clicked;
    }

    static public bool DrawHeader(string aText, Color backgroundColor, GUILayoutOption option, bool aDefaultValue = true, bool aForceDefault = false)
    {
        EditorGUIExtensions.SetBackgroundColor(backgroundColor);

        bool value = DrawHeader(aText, option, aDefaultValue, aForceDefault);

        EditorGUIExtensions.RestoreBackgroundColor();

        return value;
    }

    public static void DrawTitle(string title)
    {
        EditorGUILayout.LabelField(title, new GUIStyle("OL Title"));
    }

    public static Rect DrawBox(string aLabel, float aHeight, GUIStyle style, Color aBackgroundColor, params GUILayoutOption[] options)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(3f);

        style.richText = true;
        style.alignment = TextAnchor.MiddleCenter;
		
        SetBackgroundColor(aBackgroundColor);
        Rect boxArea = GUILayoutUtility.GetRect(0.0f, aHeight, options);
        GUI.Box(boxArea, aLabel, style);
        RestoreBackgroundColor();
		
        GUILayout.Space(2f);
        EditorGUILayout.EndHorizontal();
		
        return boxArea;
    }

    static public void BeginContent(Color aBackgroundColor, params GUILayoutOption[] options)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(3f);
		
        GUIStyle style = new GUIStyle("TextArea");
        style.richText = true;
        style.margin = new RectOffset(0, 0, 0, 0);
		
        SetBackgroundColor(aBackgroundColor);
        EditorGUILayout.BeginVertical(style, options);
    }

    public static bool Button(string text, GUIStyle style, Color color, params GUILayoutOption[] options)
    {
        EditorGUIExtensions.SetBackgroundColor(color);
        if (GUILayout.Button(text, style, options))
        {
            return true;
        }
        EditorGUIExtensions.RestoreBackgroundColor();

        return false;
    }

    public static void BeginContent(GUIStyle style, Color aBackgroundColor, params GUILayoutOption[] options)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(3f);

        style.richText = true;
        style.margin = new RectOffset(0, 0, 0, 0);

        SetBackgroundColor(aBackgroundColor);
        EditorGUILayout.BeginVertical(style, options);
    }

    static public void EndContent()
    {
        EditorGUILayout.EndVertical();

        RestoreBackgroundColor();
		
        GUILayout.Space(2f);
        EditorGUILayout.EndHorizontal();
    }

    public static bool DrawFoldout(bool value, Color aBackgroundColor, string header, string key)
    {
        value = EditorPrefs.GetBool("FoldoutValue_" + key, false);

        SetBackgroundColor(aBackgroundColor);
        EditorGUILayout.BeginHorizontal((GUIStyle)"TE NodeBox");
        SetBackgroundColor(Color.white);
        GUILayout.Space(16f);
        value = EditorGUILayout.Foldout(value, header);

        RestoreBackgroundColor();
        EditorGUILayout.EndHorizontal();
        RestoreBackgroundColor();
        EditorPrefs.SetBool("FoldoutValue_" + key, value);

        return value;
    }

    public static bool DrawFoldoutAuto(string header, string key, GUIStyle style, Color aBackgroundColor, int textSize)
    {
        bool value = EditorPrefs.GetBool("FoldoutValue_" + key, false);

        SetBackgroundColor(aBackgroundColor);
        EditorGUILayout.BeginHorizontal(style);
        SetBackgroundColor(Color.white);
        GUILayout.Space(16f);

        value = EditorGUILayout.Foldout(value, header);

        RestoreBackgroundColor();
        EditorGUILayout.EndHorizontal();

        RestoreBackgroundColor();

        EditorPrefs.SetBool("FoldoutValue_" + key, value);

        return value;
    }

    public static void SetEditorPrefColor(string aKey, Color aColor)
    {
        EditorPrefs.SetFloat(aKey + "_r", aColor.r);
        EditorPrefs.SetFloat(aKey + "_g", aColor.g);
        EditorPrefs.SetFloat(aKey + "_b", aColor.b);
        EditorPrefs.SetFloat(aKey + "_a", aColor.a);
    }

    public static Color GetEditorPrefColor(string aKey)
    {
        Color color = Color.white;
        color.r = EditorPrefs.GetFloat(aKey + "_r", 1f);
        color.g = EditorPrefs.GetFloat(aKey + "_g", 1f);
        color.b = EditorPrefs.GetFloat(aKey + "_b", 1f);
        color.a = EditorPrefs.GetFloat(aKey + "_a", 1f);
        return color;
    }


	

    private static Stack<Color> s_BackgroundColors = new Stack<Color>();

    public static void SetBackgroundColor(Color aBackground)
    {
        s_BackgroundColors.Push(GUI.backgroundColor);
        GUI.backgroundColor = aBackground;
    }

    public static void RestoreBackgroundColor()
    {
        GUI.backgroundColor = s_BackgroundColors.Pop();
    }

    public static int DrawBitMaskField(Rect aPosition, int aMask, System.Type aType, GUIContent aLabel)
    {
        var itemNames = System.Enum.GetNames(aType);
        var itemValues = System.Enum.GetValues(aType) as int[];

        int val = aMask;
        int maskVal = 0;
        for (int i = 0; i < itemValues.Length; i++)
        {
            if (itemValues[i] != 0)
            {
                if ((val & itemValues[i]) == itemValues[i])
                    maskVal |= 1 << i;
            }
            else if (val == 0)
                maskVal |= 1 << i;
        }
        int newMaskVal = EditorGUI.MaskField(aPosition, aLabel, maskVal, itemNames);
        int changes = maskVal ^ newMaskVal;

        for (int i = 0; i < itemValues.Length; i++)
        {
            if ((changes & (1 << i)) != 0)            // has this list item changed?
            {
                if ((newMaskVal & (1 << i)) != 0)     // has it been set?
                {
                    if (itemValues[i] == 0)           // special case: if "0" is set, just set the val to 0
                    {
                        val = 0;
                        break;
                    }
                    else
                        val |= itemValues[i];
                }
                else                                  // it has been reset
                {
                    val &= ~itemValues[i];
                }
            }
        }
        return val;
    }
}





