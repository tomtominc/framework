﻿using UnityEngine;
using System.Collections;
using System;

namespace Framework
{
    [AttributeUsage( AttributeTargets.Field, Inherited = true, AllowMultiple = true )]
    public class InterfaceAttachmentAttribute : PropertyAttribute 
	{
        public Type IType;

        public InterfaceAttachmentAttribute ( Type IType )
        {
            this.IType = IType;
        }
	}
}