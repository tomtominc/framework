﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Framework
{
    [CustomPropertyDrawer( typeof( InterfaceAttachmentAttribute ) )]
    public class InterfaceAttributeDrawer : DecoratorDrawer 
	{
        private InterfaceAttachmentAttribute IAttachment { get { return attribute as InterfaceAttachmentAttribute; } }
        public int selectedIndex = 0;
        public Type selectedType;
        public IEnumerable < Type > types;

        public override void OnGUI(Rect position)
        {
            if ( types == null )
            {
                types = GetTypes ();
            }

            var rect = position;

            selectedIndex = EditorGUI.Popup ( rect, selectedIndex , types.Select ( x => x.Name ).ToArray () );

            selectedType = types.ToList () [ selectedIndex ];

            var fields = selectedType.GetFields ();

            foreach ( var field in fields )
            {
                var name = field.Name;
                var temp = field.GetValue (null);
                rect.y += 16;

                if ( temp is int )
                {
                    temp = EditorGUI.IntField ( rect , name, (int)temp );
                }
                else if ( temp is string )
                {
                    temp = EditorGUI.TextField ( rect , name, (string)temp );
                }
                else if ( temp is float )
                {
                    temp = EditorGUI.FloatField ( rect , name, (float)temp );
                }
                else if ( temp is LayerMask )
                {
                    temp = EditorGUI.LayerField ( rect , name, (int)temp );
                }
                else if ( temp is Vector2 )
                {
                    temp = EditorGUI.Vector2Field ( rect , name,  (Vector2)temp );
                }
                else if ( temp.GetType ().IsEnum )
                {
                    temp = EditorGUI.EnumPopup ( rect , name, (System.Enum)temp );
                }
            }

        }

        public override float GetHeight()
        {
            if ( types == null )
            {
                return base.GetHeight ();
            }

            return 16f + ( selectedType.GetFields ().Length * 16f );
        }

        public IEnumerable < Type > GetTypes ()
        {
            var type = IAttachment.IType;

            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p));
        }
	}
}