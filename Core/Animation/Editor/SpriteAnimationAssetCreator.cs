﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.ProjectWindowCallback;
using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class SpriteAnimationAssetCreation
{
    public const float DEFAULT_FRAME_TIME = 0.066666F;

    [MenuItem("Assets/Create/SpriteAnimationAsset")]
    public static void CreateCustom()
    {
        var asset = ScriptableObject.CreateInstance<SpriteAnimationAsset>();
        ProjectWindowUtil.StartNameEditingIfProjectWindowExists(
            asset.GetInstanceID(),
            ScriptableObject.CreateInstance<EndSpriteAnimationAssetNameEdit>(),
            "SpriteAnimationAsset.asset",
            AssetPreview.GetMiniThumbnail(asset), 
            null);
			
    }

    [MenuItem("Assets/Create/Sprite Animation")]
    public static void CreateSpriteAnimation()
    {
        Texture2D l_activeSelection = Selection.activeObject as Texture2D;

        if (null == l_activeSelection)
            return;
        
        string l_name = l_activeSelection.name.Split('_')[0];

        if (!l_name.Contains("@"))
            return;

        string l_animationAssetName = l_name.Split('@')[0];
        string l_animationName = l_name.Split('@')[1];
        string l_animationAssetPath = getAnimationAssetSavePath() + l_animationAssetName + ".asset";
        SpriteAnimationAsset l_animationAsset = AssetDatabase.LoadAssetAtPath<SpriteAnimationAsset>(l_animationAssetPath);

        if (l_animationAsset == null)
        {
            l_animationAsset = ScriptableObject.CreateInstance<SpriteAnimationAsset>();
            l_animationAsset.name = l_animationAssetName;
            l_animationAsset.animations = new List<SpriteAnimationData>();
            AssetDatabase.CreateAsset(l_animationAsset, AssetDatabase.GenerateUniqueAssetPath(l_animationAssetPath));
        }

        SpriteAnimationData l_animationData = new SpriteAnimationData();
        l_animationData.name = l_animationName;
        l_animationData.frameDatas = new List<SpriteAnimationFrameData>();

        Sprite[] l_sprites = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(l_activeSelection)).OfType<Sprite>().ToArray();

        for (int i = 0; i < l_sprites.Length; i++)
        {
            Sprite l_sprite = l_sprites[i];
            SpriteAnimationFrameData l_frameData = new SpriteAnimationFrameData();
            l_frameData.time = DEFAULT_FRAME_TIME;
            l_frameData.sprite = l_sprite;
            l_animationData.frameDatas.Add(l_frameData);
        }

        l_animationAsset.animations.Add(l_animationData);

    }

    private static string getAnimationAssetSavePath()
    {
        string l_path = "Assets/Source/Animations/";

        if (!Directory.Exists(l_path))
        {
            l_path = AssetDatabase.GetAssetPath(Selection.activeObject);

            if (l_path == "")
            {
                l_path = "Assets";
            }
            else if (Path.GetExtension(l_path) != "")
            {
                l_path = l_path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }
        }

        return  l_path;

    }
}

internal class EndSpriteAnimationAssetNameEdit : EndNameEditAction
{
    public override void Action(int InstanceID, string path, string file)
    {
        AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(InstanceID), AssetDatabase.GenerateUniqueAssetPath(path));
    }
}
