﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab.ClientModels;
using System.Linq;
using UnityEngine.Purchasing;

public class IAPItem : StoreItem
{
    #region Members

    protected Product m_product;

    #endregion

    public IAPItem(CatalogItem p_item, EconomyService p_economyService)
        : base(p_item, p_economyService)
    {
    }

    public override string id()
    {
        if (null != m_product)
            return m_product.definition.id;
        
        return base.id();
    }

    public override string name()
    {
        if (null != m_product)
            return m_product.metadata.localizedTitle;
        
        return base.name();
    }

    public override string description()
    {
        if (null != m_product)
            return m_product.metadata.localizedDescription;
        
        return base.description();
    }

    public override string price()
    {
        if (null != m_product)
            return m_product.metadata.localizedPriceString;

        return base.price();
    }

    public void setProduct(Product p_product)
    {
        m_product = p_product;
    }

    public override void purchase()
    {
        
    }
}
