﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab.ClientModels;
using System.Linq;

public class StoreItem : IItem
{
    #region Members

    protected CatalogItem m_item;
    protected EconomyService m_economyService;

    #endregion

    public StoreItem(CatalogItem p_item, EconomyService p_economyService)
    {
        m_item = p_item;
        m_economyService = p_economyService;
    }

    public virtual string id()
    {
        return m_item.ItemId;
    }

    public virtual string name()
    {
        return m_item.DisplayName;
    }

    public virtual string description()
    {
        return m_item.Description;
    }

    public virtual string price()
    {
        if (currencies().Count <= 0)
            return "???";
        
        var l_value = currencies().ElementAt(0);
        return string.Format("{0} {1}", l_value.Value, l_value.Key);
    }

    public virtual Dictionary<string,uint> currencies()
    {
        return m_item.VirtualCurrencyPrices;
    }

    public virtual string type()
    {
        return m_item.ItemClass;
    }

    public virtual List <string> tag()
    {
        return m_item.Tags;
    }

    public virtual bool isStackable()
    {
        return m_item.IsStackable;
    }

    public virtual void purchase()
    {
        
    }
}
