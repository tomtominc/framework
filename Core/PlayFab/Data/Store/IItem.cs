﻿using System.Collections.Generic;


public interface IItem
{
    string id();

    string name();

    string description();

    string price();

    Dictionary<string,uint> currencies();

    string type();

    List <string> tag();

    bool isStackable();

    void purchase();
}
