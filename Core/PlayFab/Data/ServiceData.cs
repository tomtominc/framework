﻿using Newtonsoft.Json;
using PlayFab;
using PlayFab.ServerModels;
using UnityEngine;
using System.Collections.Generic;

public class ServiceData<T>
{
    protected virtual string m_dataKey
    {
        get { return "NULL"; }
    }

    public virtual void updateServerData()
    {
        SetTitleDataRequest l_updateRequest = new SetTitleDataRequest();
        l_updateRequest.Key = m_dataKey;
        l_updateRequest.Value = ToString();

        PlayFabServerAPI.SetTitleData(l_updateRequest, onSetTitleData, onFailedSetTitleData);
    }

    #region Callbacks

    protected virtual void onSetTitleData ( SetTitleDataResult p_result )
    {
        Debug.LogFormat("Updated {0}", m_dataKey);
    }

    protected virtual void onFailedSetTitleData ( PlayFabError p_error )
    {
        Debug.LogWarningFormat("Error: {0}", p_error);
    }
    #endregion

    #region Overrides

    public override string ToString()
    {
        return JsonConvert.SerializeObject(this);
    }
    #endregion

    #region Private Methods
    
    #endregion

    #region Static Methods

    public static T create(string p_json)
    {
        return JsonConvert.DeserializeObject<T>(p_json);
    }

    #endregion
}
