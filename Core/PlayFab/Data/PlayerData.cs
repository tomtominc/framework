﻿using System.Collections.Generic;
using PlayFab.ClientModels;
using Newtonsoft.Json;
using System.Linq;

public class PlayerData
{
    protected GameServiceProvider m_serviceProvider;

    protected string m_id;
    protected string m_sessionTicket;

    protected UserAccountInfo m_accountInfo;
    protected PlayerProfileModel m_profile;
    protected Dictionary<string, string> m_data;
    protected List<ItemInstance> m_inventory;
    protected Dictionary<string, int> m_currencies;

    protected bool m_newlyCreatedAccount;
    protected System.DateTime? m_lastLoginTime;

    public bool newlyCreatedAccount
    {
        get { return m_newlyCreatedAccount; }
    }

    public void setGameServiceProvider(GameServiceProvider p_serviceProvider)
    {
        m_serviceProvider = p_serviceProvider;    
    }

    public void setPlayerData(LoginResult p_loginResult)
    {
        m_id = p_loginResult.PlayFabId;
        m_sessionTicket = p_loginResult.SessionTicket;

        m_accountInfo = p_loginResult.InfoResultPayload.AccountInfo;
        m_profile = p_loginResult.InfoResultPayload.PlayerProfile;
        m_data = p_loginResult.InfoResultPayload.UserData.ToDictionary(k => k.Key, k => k.Value.Value);
        m_inventory = p_loginResult.InfoResultPayload.UserInventory;
        m_currencies = p_loginResult.InfoResultPayload.UserVirtualCurrency;

        m_newlyCreatedAccount = p_loginResult.NewlyCreated;
        m_lastLoginTime = p_loginResult.LastLoginTime;

        configureData();
    }

    protected virtual void configureData()
    {
        
    }

    protected T create<T>(string p_json)
    {
        return JsonConvert.DeserializeObject<T>(p_json);
    }

    public virtual Dictionary<string,string> getSaveData()
    {
        return null;
    }
}
