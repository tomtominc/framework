﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    protected GameServiceProvider m_gameServiceProvider;
    protected PlayerService m_playerService;

    protected PlayerData m_playerData
    {
        get { return m_playerService.playerData; }
    }

    public virtual void init(GameServiceProvider p_gameServiceProvider)
    {
        m_gameServiceProvider = p_gameServiceProvider;    
        m_playerService = m_gameServiceProvider.playerService;
    }
}
