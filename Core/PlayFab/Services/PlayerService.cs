﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class PlayerService : GameService
{
    [SerializeField]
    protected string m_editorCustomId = "EDITOR_PLAYER_0";

    protected PlayerData m_playerData;

    public PlayerData playerData
    {
        get { return m_playerData; }
    }

    public override void updateData()
    {
        UpdateUserDataRequest l_updateUser = new UpdateUserDataRequest();
        l_updateUser.Data = playerData.getSaveData();
        PlayFabClientAPI.UpdateUserData(l_updateUser, handleUpdateSuccess, handleUpdateFailure);
    }

    public override void requestData()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            var l_request = new LoginWithIOSDeviceIDRequest();
            l_request.CreateAccount = true;
            l_request.TitleId = GameServiceProvider.TITLE_ID;
            l_request.DeviceId = SystemInfo.deviceUniqueIdentifier;
            l_request.DeviceModel = SystemInfo.deviceModel;

            l_request.InfoRequestParameters.GetUserAccountInfo = true;
            l_request.InfoRequestParameters.GetUserData = true;
            l_request.InfoRequestParameters.GetUserInventory = true;
            l_request.InfoRequestParameters.GetUserVirtualCurrency = true;

            PlayFabClientAPI.LoginWithIOSDeviceID(l_request, handleRequestSuccess, handleRequestFail);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            var l_request = new LoginWithAndroidDeviceIDRequest();

            l_request.CreateAccount = true;
            l_request.TitleId = GameServiceProvider.TITLE_ID;
            l_request.AndroidDeviceId = SystemInfo.deviceUniqueIdentifier;
            l_request.AndroidDevice = SystemInfo.deviceModel;

            l_request.InfoRequestParameters.GetUserAccountInfo = true;
            l_request.InfoRequestParameters.GetUserData = true;
            l_request.InfoRequestParameters.GetUserInventory = true;
            l_request.InfoRequestParameters.GetUserVirtualCurrency = true;

            PlayFabClientAPI.LoginWithAndroidDeviceID(l_request, handleRequestSuccess, handleRequestFail);
        }
        else
        {
            var l_request = new LoginWithCustomIDRequest();

            l_request.TitleId = GameServiceProvider.TITLE_ID;
            l_request.CustomId = m_editorCustomId;
            l_request.CreateAccount = true;

            l_request.InfoRequestParameters = new GetPlayerCombinedInfoRequestParams();
            l_request.InfoRequestParameters.GetUserAccountInfo = true;
            l_request.InfoRequestParameters.GetUserData = true;
            l_request.InfoRequestParameters.GetUserInventory = true;
            l_request.InfoRequestParameters.GetUserVirtualCurrency = true;

            PlayFabClientAPI.LoginWithCustomID(l_request, handleRequestSuccess, handleRequestFail);
        }
    }
}
