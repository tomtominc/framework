﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.SharedModels;
using PlayFab.ClientModels;
using Sirenix.OdinInspector;
using System;

public class GameServiceProvider : SingletonBehaviour<GameServiceProvider>
{
    public const string TITLE_ID = "D984";

    [SerializeField]
    private TitleService m_titleService;
    [SerializeField]
    private PlayerService m_playerService;
    [SerializeField]
    private EconomyService m_economyService;
    [SerializeField]
    private TimeService m_timeService;
    [SerializeField]
    private ChestManager m_chestManager;
    [SerializeField]
    private SpriteManager m_spriteManager;

    public event Action<PlayFabResultCommon> requestTitleDataSuccess = delegate {};
    public event Action<PlayFabResultCommon> requestPlayerDataSuccess = delegate {};
    public event Action<PlayFabResultCommon> requestEconomyDataSuccess = delegate {};

    public event Action<PlayFabError> requestTitleDataFailure = delegate {};
    public event Action<PlayFabError> requestPlayerDataFailure = delegate {};
    public event Action<PlayFabError> requestEconomyDataFailure = delegate {};

    public event Action<PlayFabResultCommon> updatePlayerDataSuccess = delegate {};
    public event Action<PlayFabError> updatePlayerDataFailure = delegate {};

    #region Properties

    public PlayerService playerService
    {
        get { return m_playerService; }
    }

    public TitleService titleService
    {
        get { return m_titleService; }
    }

    public EconomyService economyService
    {
        get { return m_economyService; }
    }

    public TimeService timeService
    {
        get { return m_timeService; }
    }

    public ChestManager chestManager
    {
        get { return m_chestManager; }
    }

    public SpriteManager spriteManager
    {
        get { return m_spriteManager; }
    }

    #endregion

    public void Initialize()
    {
        // Services
        m_titleService.Initialize(this);
        m_playerService.Initialize(this);
        m_economyService.Initialize(this);
        m_timeService.Initialize(this);

        //Managers
        m_chestManager.init(this);

        setupServiceHooks();
    }

    #region Service Hooks

    private void setupServiceHooks()
    {
        m_titleService.requestSuccess += onRequestTitleDataSuccess;
        m_titleService.requestFail += onRequestTitleDataFailed;

        m_playerService.requestSuccess += onRequestPlayerDataSuccess;
        m_playerService.requestFail += onRequestPlayerDataFailed;
        m_playerService.updateSuccess += onUpdatePlayerDataSuccess;
        m_playerService.updateFailure += onUpdatePlayerDataFailed;

        m_economyService.requestSuccess += onRequestEconomyDataSuccess;
        m_economyService.requestFail += onRequestEconomyDataFailed;
    }

    #endregion

    #region Request Data

    public void requestTitleData()
    {
        m_titleService.requestData();
    }

    public void requestPlayerData()
    {
        m_playerService.requestData();
    }

    public void requestEconomyData()
    {
        m_economyService.requestData();
    }

    public void requestTime(Action<DateTime> p_onGetTime, Action<int,string> p_onError)
    {
        m_timeService.getTime(p_onGetTime, p_onError);
    }

    #endregion

    #region Update Data

    public void updatePlayerData()
    {
        m_playerService.updateData();
    }

    #endregion

    #region Callbacks

    private void onRequestTitleDataSuccess(PlayFabResultCommon p_result)
    {
        requestTitleDataSuccess(p_result);
    }

    private void onRequestTitleDataFailed(PlayFabError p_error)
    {
        requestTitleDataFailure(p_error);
    }

    private void onRequestPlayerDataSuccess(PlayFabResultCommon p_result)
    {
        requestPlayerDataSuccess(p_result);
    }

    private void onRequestPlayerDataFailed(PlayFabError p_error)
    {
        requestPlayerDataFailure(p_error);
    }

    private void onRequestEconomyDataSuccess(PlayFabResultCommon p_result)
    {
        requestEconomyDataSuccess(p_result);
    }

    private void onRequestEconomyDataFailed(PlayFabError p_error)
    {
        requestEconomyDataFailure(p_error);
    }

    private void onUpdatePlayerDataSuccess(PlayFabResultCommon p_result)
    {
        updatePlayerDataSuccess(p_result);
    }

    private void onUpdatePlayerDataFailed(PlayFabError p_error)
    {
        updatePlayerDataFailure(p_error);
    }

    #endregion
}
