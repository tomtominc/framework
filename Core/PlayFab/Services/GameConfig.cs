﻿using System.Collections.Generic;

public class GameConfig
{
    public static List<string> configKeys;

    protected Dictionary<string, string> m_data;

    public void setConfig(Dictionary <string,string> p_data)
    {
        m_data = p_data;
        configureConfig();
    }

    protected virtual void configureConfig()
    {

    }

    public virtual string getValue()
    {
        return string.Empty;
    }
}
