﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.SharedModels;
using System;

public class TimeService : GameService
{
    private event Action<DateTime> m_getTime = delegate {};

    public override void requestData()
    {
        
    }

    public void getTime(Action<DateTime> p_onGetTime, Action<int,string> p_onError)
    {
        m_getTime += p_onGetTime;
        m_error += p_onError;

        GetTimeRequest l_timeRequest = new GetTimeRequest();
        PlayFabClientAPI.GetTime(l_timeRequest, handleRequestSuccess, handleRequestFail);
    }

    protected override void handleRequestSuccess(PlayFabResultCommon p_result)
    {
        GetTimeResult l_result = p_result as GetTimeResult;
        m_getTime(l_result.Time);

        m_getTime = delegate{};
        resetErrorDelegate();
    }
}
