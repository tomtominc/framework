﻿using UnityEngine;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ServerModels;
using System;
using Newtonsoft.Json;

public class TitleService : GameService
{
    #region Members

    protected List<string> m_configDataKeys;

    #endregion

    public override void requestData()
    {
        GetTitleDataRequest l_request = new GetTitleDataRequest();
        l_request.Keys = m_configDataKeys;

        PlayFabServerAPI.GetTitleData(l_request, handleRequestSuccess, handleRequestFail);
    }

}
