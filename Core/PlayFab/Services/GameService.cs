﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using System;
using PlayFab.SharedModels;

public abstract class GameService : MonoBehaviour
{
    #region events

    public event Action<PlayFabResultCommon> requestSuccess = delegate { };
    public event Action<PlayFabError> requestFail = delegate { };

    public event Action<PlayFabResultCommon> updateSuccess = delegate { };
    public event Action<PlayFabError> updateFailure = delegate { };

    protected event Action <int,string> m_error = delegate {};

    #endregion

    #region Members

    protected GameServiceProvider m_gameServiceProvider;

    #endregion

    #region Methods

    public virtual void Initialize(GameServiceProvider p_gameServiceProvider)
    {
        m_gameServiceProvider = p_gameServiceProvider;
    }

    public abstract void requestData();

    public virtual void updateData()
    {
    }

    protected void resetErrorDelegate()
    {
        m_error = delegate{};
    }

    #endregion

    #region Callbacks

    protected virtual void handleRequestSuccess(PlayFabResultCommon p_result)
    {
        requestSuccess(p_result);
    }

    protected virtual void handleRequestFail(PlayFabError p_error)
    {
        int p_errorCode = (int)p_error.Error;
        string p_errorMessage = p_error.GenerateErrorReport();
        m_error(p_errorCode, p_errorMessage);
    }

    protected virtual void handleUpdateSuccess(PlayFabResultCommon p_result)
    {
        updateSuccess(p_result);
    }

    protected virtual void handleUpdateFailure(PlayFabError p_error)
    {
        Debug.LogWarningFormat("Error: {0}", p_error);
        updateFailure(p_error);
    }

    #endregion
}
