﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab.ClientModels;
using PlayFab;
using PlayFab.SharedModels;
using System;

public class EconomyService : GameService
{
    [SerializeField] private string m_catalogVersion = "0.0.1";

    private List < IItem > m_storeItems;

    #region Properties

    public List < IItem > storeItems
    {
        get { return m_storeItems; }
    }

    #endregion

    public override void requestData()
    {
        m_storeItems = new List<IItem>();

        GetCatalogItemsRequest l_catalogRequest = new GetCatalogItemsRequest();
        l_catalogRequest.CatalogVersion = m_catalogVersion;
        PlayFabClientAPI.GetCatalogItems(l_catalogRequest, handleRequestSuccess, handleRequestFail);
    }

    #region Event Callbacks

    protected override void handleRequestSuccess(PlayFabResultCommon p_result)
    {
        GetCatalogItemsResult l_result = p_result as GetCatalogItemsResult;

        List < CatalogItem > l_catalogItems = l_result.Catalog;

        foreach (CatalogItem l_catalogItem in l_catalogItems)
        {
            IItem l_item = null;

            if (l_catalogItem.VirtualCurrencyPrices.ContainsKey("RM"))
                l_item = new IAPItem(l_catalogItem, this);
            else
                l_item = new StoreItem(l_catalogItem, this);

            m_storeItems.Add(l_item);
        }

        base.handleRequestSuccess(p_result);
    }

    #endregion
}
