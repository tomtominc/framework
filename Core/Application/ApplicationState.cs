﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class ApplicationState<T> : MonoBehaviour where T : IConvertible
{
    protected ApplicationManager<T> m_applicationManager;

    public virtual void initialize(ApplicationManager<T> p_applicationManager)
    {
        m_applicationManager = p_applicationManager;
    }

    public abstract void enter();

    public abstract void update();

    public abstract void exit();
}
