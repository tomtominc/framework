﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public struct TransitionData
{
    public int index;
    public float inDuration;
    public float outDuration;
    public string scene;

    public TransitionData(string scene)
    {
        this.index = -1;
        this.inDuration = 0.5f;
        this.outDuration = 0.5f;
        this.scene = scene;
    }

    public TransitionData(string scene, int index)
    {
        this.index = index;
        this.inDuration = 0.5f;
        this.outDuration = 0.5f;
        this.scene = scene;
    }
}

/// <summary>
/// To render over UI images make sure this camera is set to only render UI
/// and has a depth that's greater than any other camera.
/// </summary>

public class TransitionCamera : MonoBehaviour
{
    public Material TransitionMaterial;

    [Range(0f, 1f)]
    public float cutoff = 0f;
    private float lastCutoff = 0f;

    public List < Texture2D > textures;

    void OnEnable()
    {
        cutoff = 0;
        TransitionMaterial.SetFloat("_Cutoff", cutoff);
    }

    void Update()
    {
        if (TransitionMaterial != null && cutoff != lastCutoff)
        {
            TransitionMaterial.SetFloat("_Cutoff", cutoff);
            lastCutoff = cutoff;
        }
    }

    void OnDisable()
    {
        cutoff = 0;
        TransitionMaterial.SetFloat("_Cutoff", cutoff);
    }

    void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (TransitionMaterial != null)
        {
            Graphics.Blit(src, dst, TransitionMaterial);
        }


    }

    public void DoTransitionEffect(float duration, float value, Ease ease = Ease.Linear, int transitionIndex = -1)
    {
        if (TransitionMaterial == null)
        {
            Debug.LogWarning("Transition material is null, you need to apply one to the variable in the inspector.");
            return;
        }

        if (textures.Count <= transitionIndex)
        {
            Debug.LogWarningFormat("Invalid index: Textures.Count = {0} Transition Index = {1}", textures.Count, transitionIndex);
            return;
        }

        if (transitionIndex > -1)
        {
            TransitionMaterial.SetTexture("_TransitionTex", textures[transitionIndex]);
        }

        TransitionMaterial.DOFloat(value, "_Cutoff", duration).SetEase(ease);
    }
}
