﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class EventManager
{
    private static Dictionary<System.Enum, MessageHandler> m_messageHandlers;

    public static void initialize()
    {
        m_messageHandlers = new Dictionary<System.Enum, MessageHandler>();
    }

    public static void clearListeners()
    {
        m_messageHandlers.Clear();
    }

    public static void subscribe(System.Enum p_message, MessageHandler p_handler)
    {
        MessageListenerDefinition l_listener = MessageListenerDefinition.allocate();
        l_listener.messageType = p_message;
        l_listener.handler = p_handler;
        subscribe(l_listener);
    }

    private static void subscribe(MessageListenerDefinition p_listener)
    {
        if (m_messageHandlers.ContainsKey(p_listener.messageType))
        {
            m_messageHandlers[p_listener.messageType] += p_listener.handler;
        }
        else
        {
            m_messageHandlers.Add(p_listener.messageType, p_listener.handler);
        }

        MessageListenerDefinition.release(p_listener);
    }

    public static void unsubscribe(System.Enum p_message, MessageHandler p_handler)
    {
        MessageListenerDefinition l_listener = MessageListenerDefinition.allocate();
        l_listener.messageType = p_message;
        l_listener.handler = p_handler;
        unsubscribe(l_listener);
    }

    private static void unsubscribe(MessageListenerDefinition p_listener)
    {
        if (m_messageHandlers.ContainsKey(p_listener.messageType))
        {
            MessageHandler l_handler = m_messageHandlers[p_listener.messageType];
            l_handler -= p_listener.handler;

            if (null == l_handler)
                m_messageHandlers.Remove(p_listener.messageType);
        }
        MessageListenerDefinition.release(p_listener);
    }

    public static void publish(object p_sender, System.Enum p_message, object p_data)
    {
        Message l_message = Message.allocate();
        l_message.sender = p_sender;
        l_message.messageType = p_message;
        l_message.data = p_data;

        publish(l_message);

        l_message.release();
    }

    private static void publish(IMessage p_message)
    {
        if (m_messageHandlers.ContainsKey(p_message.messageType))
        {
            MessageHandler l_handler = m_messageHandlers[p_message.messageType];
            l_handler(p_message);
        }
    }
}

public delegate void MessageHandler(IMessage rMessage);

