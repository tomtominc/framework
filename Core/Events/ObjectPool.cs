using System;
using System.Collections.Generic;

public sealed class ObjectPool<T> where T : new()
{
    private int m_growSize = 20;
    private T[] m_pool;
    private int m_nextIndex = 0;

    public ObjectPool(int p_size)
    {
        resize(p_size, false);
    }

    public ObjectPool(int p_size, int p_growSize)
    {
        m_growSize = p_growSize;
        resize(p_size, false);
    }

    public int length
    {
        get { return m_pool.Length; }
    }

    public int available
    {
        get { return m_pool.Length - m_nextIndex; }
    }

    public int allocated
    {
        get { return m_nextIndex; }
    }

    public T allocate()
    {
        T l_item = default(T);

        if (m_nextIndex >= m_pool.Length)
        {
            if (m_growSize > 0)
            {
                resize(m_pool.Length + m_growSize, true);
            }
            else
            {
                return l_item;
            }
        }
        if (m_nextIndex >= 0 && m_nextIndex < m_pool.Length)
        {
            l_item = m_pool[m_nextIndex];
            m_nextIndex++;
        }
			
        return l_item;
    }

    public void release(T p_instance)
    {
        if (m_nextIndex > 0)
        {
            m_nextIndex--;
            m_pool[m_nextIndex] = p_instance;
        }
    }

    public void reset()
    {
        int l_length = m_growSize;
        if (m_pool != null)
        {
            l_length = m_pool.Length;
        }
        resize(l_length, false);
        m_nextIndex = 0;
    }

    public void resize(int p_size, bool p_copyExisting)
    {
        lock (this)
        {
            int l_count = 0;
            T[] l_pool = new T[p_size];
				
            if (m_pool != null && p_copyExisting)
            {
                l_count = m_pool.Length;
                Array.Copy(m_pool, l_pool, Math.Min(l_count, p_size));
            }

            for (int i = l_count; i < p_size; i++)
            {
                l_pool[i] = new T();
            }
            m_pool = l_pool;
        }
    }
}

