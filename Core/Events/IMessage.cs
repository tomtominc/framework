﻿
public interface IMessage
{
    System.Enum messageType { get; set; }

    object sender { get; set; }

    object data { get; set; }

    void clear();

    void release();
}

