﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using Framework.EditorTools;

[RequireComponent(typeof(GridLayoutGroup))]
public class UISpriteText : MonoBehaviour
{
    [ButtonAttribute("Reset Text", "InitText")]
   
    [SerializeField]private bool capsLock = true;
    [SerializeField]private string _intialText;
    [SerializeField]private UISpriteFont _spriteFont;

    private GridLayoutGroup _gridLayoutGroup;
    
    private string _text;

    public string Text
    {
        get
        {
            return _text;        
        }

        set
        {
            _text = value;

            SetText(_text);
        }
    }

    public void InitText()
    {
        Text = _intialText;
    }

    private void Initialize()
    {
        _gridLayoutGroup = GetComponent < GridLayoutGroup >();
    }

    private void SetText(string text)
    {
        if (_gridLayoutGroup == null)
            Initialize();
        
        if (capsLock)
            text = text.ToUpper();
        
        char[] array = text.ToCharArray();

        if (_gridLayoutGroup.startCorner == GridLayoutGroup.Corner.UpperRight ||
            _gridLayoutGroup.startCorner == GridLayoutGroup.Corner.LowerRight)
            Array.Reverse(array);

        text = new string(array);

        RemoveExcessiveChildren(array.Length);
        for (int i = 0; i < text.Length; i++)
        {
            char c = text[i];

            if (c.Equals('_'))
                c = ' ';

            Sprite sprite = _spriteFont.GetSprite(c.ToString());
            SetSprite(i, sprite);
        }
    }

    public void setColor(Color p_color)
    {
        foreach (Transform child in transform)
        {
            child.GetComponent < Image >().color = p_color;
        }
    }

    private void SetSprite(int childIndex, Sprite sprite)
    {
        if (childIndex >= transform.childCount)
        {
            CreateChild();
        }

        Transform child = transform.GetChild(childIndex);

        if (child == null)
            return;

        child.gameObject.SetActive(true);

        Image image = child.GetComponent < Image >();

        if (image == null)
            return;
        
        image.sprite = sprite;
    }

    private void RemoveExcessiveChildren(int maxCount)
    {
        if (transform.childCount <= maxCount)
            return;

        for (int i = maxCount; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    private void CreateChild()
    {
        GameObject go = new GameObject("child", typeof(Image));
        go.transform.SetParent(transform, false);
        go.GetComponent < Image >().raycastTarget = false;
    }

}
