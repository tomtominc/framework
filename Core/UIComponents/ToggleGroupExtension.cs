﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(ToggleGroup))]
public class ToggleGroupExtension : MonoBehaviour
{
    [SerializeField]
    protected ToggleExtension m_initalToggle;
    [SerializeField]
    protected List < ToggleExtension > m_navigations;

    protected IContentToggle m_contentToggle;
    protected RectTransform m_rectTransform;
    protected ToggleGroup m_toggleGroup;

    public virtual void init(IContentToggle p_contentToggle)
    {
        m_contentToggle = p_contentToggle;
        m_rectTransform = GetComponent < RectTransform  >();
        m_toggleGroup = GetComponent < ToggleGroup >();
    }

    protected virtual void initToggles()
    {
        m_navigations.ForEach(nav => nav.init(this));
        m_initalToggle.forceToggle(true);
    }

    public virtual void toggleTriggered(ToggleExtension p_toggle)
    {
        if (null != m_contentToggle)
            m_contentToggle.updateContent(m_navigations.IndexOf(p_toggle));
        
        foreach (ToggleExtension l_toggle in m_navigations)
        {
            if (!l_toggle.Equals(p_toggle))
                l_toggle.onToggleOff();
        }
    }
}
