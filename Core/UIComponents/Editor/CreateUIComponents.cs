﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateUIComponents : MonoBehaviour
{
    [MenuItem("Assets/Create/Sprite Font")]
    public static void CreateUISpriteFont()
    {
        ScriptableObjectUtility.CreateAsset < UISpriteFont >();
    }
}
