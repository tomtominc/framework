﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class UIEnergyBar : MonoBehaviour
{
    #region Members

    [SerializeField]private Slider m_slider;

    [SerializeField]private UISpriteText m_label;
    [SerializeField]private string m_format = "{0}/{1}";

    [SerializeField]private int m_minValue;
    [SerializeField]private int m_maxValue;

    [SerializeField]private float m_speed = 0.1f;

    [SerializeField]private Ease m_fillEase;
    [SerializeField]private Ease m_unfillEase;

    private int m_value;

    #endregion

    #region Properties

    public virtual int value
    {
        get { return m_value; }
    }

    public virtual int maxValue
    {
        get { return m_maxValue; }
    }

    public virtual int minValue
    {
        get { return m_minValue; }
    }

    public virtual Slider slider
    {
        get { return m_slider; }
    }

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        RefreshLabel(0);
        RefreshSlider();
    }

    #endregion

    #region Public Methods

    public virtual void SetSliderValues(int p_min, int p_max, int p_value)
    {
        m_minValue = p_min;
        m_maxValue = p_max;
        m_value = p_value;

        RefreshSlider();
    }

    public virtual void RefreshLabel(int p_value)
    {
        m_value = p_value;

        if (m_label == null)
            return;
        m_label.Text = string.Format(m_format, p_value, m_maxValue, m_minValue);
    }

    public virtual void RefreshSlider()
    {
        if (m_slider == null)
            return;
        
        m_slider.minValue = m_minValue;
        m_slider.maxValue = m_maxValue;
        m_slider.value = m_value;
    }

    public virtual void FillValue(int p_value, Action onComplete = null)
    {
        int l_value = Mathf.Clamp(m_value + p_value, m_minValue, m_maxValue);
        float l_duration = Mathf.Abs(l_value) * m_speed;
        Ease l_ease = l_value > m_value ? m_fillEase : m_unfillEase;

        m_value = l_value;

        m_slider.DOValue(m_value, l_duration)
            .SetEase(l_ease)
            .OnUpdate(() => RefreshLabel((int)m_slider.value))
            .OnComplete(() =>
            {
                if (onComplete != null)
                    onComplete();
            });
    }

    public virtual Tweener FillValueTween(int p_value)
    {
        int l_value = Mathf.Clamp(m_value + p_value, m_minValue, m_maxValue);
        float l_duration = Mathf.Abs(l_value) * m_speed;
        Ease l_ease = l_value > m_value ? m_fillEase : m_unfillEase;

        m_value = l_value;

        Tweener l_tween = m_slider.DOValue(m_value, l_duration)
            .SetEase(l_ease)
            .OnUpdate(() => RefreshLabel((int)m_slider.value));

        return l_tween;
    }

    public virtual void FillValue(int p_value, Action <int> p_overflow)
    {
        int l_overflow = 0;

        if (m_value + p_value > m_maxValue)
        {
            l_overflow = (m_value + p_value) - m_maxValue;    
        }

        int l_value = Mathf.Clamp(m_value + p_value, m_minValue, m_maxValue);
        float l_duration = Mathf.Abs(l_value) * m_speed;
        Ease l_ease = l_value > m_value ? m_fillEase : m_unfillEase;

        m_value = l_value;

        m_slider.DOValue(m_value, l_duration)
            .SetEase(l_ease)
            .OnUpdate(() => RefreshLabel((int)m_slider.value))
            .OnComplete(() =>
            {
                if (l_overflow > 0)
                    p_overflow(l_overflow);
            });
    }

    #endregion


}
