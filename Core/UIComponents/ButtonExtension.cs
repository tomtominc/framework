﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace Framework
{
    [RequireComponent ( typeof ( Button ) ) ]
    public class ButtonExtension : MonoBehaviour, IPointerDownHandler , IPointerUpHandler
	{
        public bool useGain = false;
        public float gainSpeed = 1f;

        public float maximumInterval = 0.1f;

        public UnityEvent OnDown = new UnityEvent ();
        private bool heldDown;

        private float actualInterval = 0f;
        private float currentIntereval = 0f;

        public void OnPointerDown ( PointerEventData data )
        {
            heldDown = true;

            ResetInterval ();
        }

        public void OnPointerUp ( PointerEventData data )
        {
            heldDown = false;
        }

        private void Update ()
        {
            if ( heldDown && OnDown != null)
            {
                if ( currentIntereval >= actualInterval )
                {
                    currentIntereval = 0;
                    OnDown.Invoke ();
                }
                else
                {
                    if ( useGain )
                    {
                        actualInterval -= Time.deltaTime * gainSpeed;
                    }

                    currentIntereval += Time.deltaTime;
                }
            }
        }

        public void ResetInterval ()
        {
            actualInterval = maximumInterval;
            currentIntereval = actualInterval;
        }
	}
}