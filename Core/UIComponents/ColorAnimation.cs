﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Framework
{
    [RequireComponent (typeof( Image ) ) ]
	public class ColorAnimation : MonoBehaviour 
	{
        public bool animate = true;
        public float swapSpeed = 0.1f;
        public List < Color > colors = new List < Color > ();

        protected Image image;

        protected int currentColor = 0;

        private void OnEnable ()
        {
            currentColor = 0;

            image = GetComponent < Image > ();
            image.color = colors [ currentColor ];

            StartCoroutine ( "SwapColor" );
        }

        private void OnDisable ()
        {
            StopCoroutine ( "SwapColor" );
        }

        private IEnumerator SwapColor ()
        {
            while ( animate )
            {
                currentColor++;
                yield return new WaitForSeconds ( swapSpeed );

                if ( currentColor >= colors.Count ) currentColor = 0;

                image.color = colors [ currentColor ];
            }
        }
	}
}