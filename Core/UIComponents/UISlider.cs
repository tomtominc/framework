﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UISlider : MonoBehaviour
{
    [SerializeField] private Text _label;
    [SerializeField] private Image _slider;

    [SerializeField] private string _format = "{0}";

    [SerializeField] private int _maxValue = 1;
    [SerializeField] private int _minValue = 0;

    [SerializeField] private int _value = 1;

    public void Start()
    {
        UpdateView();
    }

    public virtual int Value
    {
        get { return _value; }
        set
        {
            _value = value;
            UpdateView();
        }
    }

    public virtual int MaxValue
    {
        get{ return _maxValue; }
    }

    public virtual int MinValue
    {
        get { return _minValue; }
    }

    public virtual float Percent
    {
        get { return (float)_value / (float)_maxValue; }
    }

    public virtual void UpdateView()
    {
        _slider.fillAmount = Percent;
        _label.text = string.Format(_format, _value, _maxValue);
    }

    public virtual void FillValue(int value, float duration)
    {
        int toValue = Value + value;
//        int overflowAmount = 0;
//
//        if (toValue >= MaxValue)
//        {
//            overflowAmount = toValue - MaxValue;
//            toValue = MaxValue;
//        }


        DOTween.To(() => Value, x => Value = x, toValue, duration).OnComplete 
        (
            () =>
            {
//                if (onOverflow != null && overflowAmount > 0)
//                    onOverflow.Invoke(overflowAmount);
            }

        );
    }
}
