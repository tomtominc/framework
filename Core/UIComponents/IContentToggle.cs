﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IContentToggle
{
    void updateContent(int p_category);
}
