﻿/*
 * Copyright 2015 Evil Wizard Studios
 * 
 * This file contains the internal logic for the Selectionator class. Public-facing functions are
 * contained in Selector.cs
 */

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Framework
{
    public partial class Selector
    {
        /// <summary>
        ///     This applies the correct bonus function based on the pivot and range of the selector.
        /// </summary>
        private IEnumerable<BonusContainer<T>> ApplyAdjust<T>(IEnumerable<BonusContainer<T>> adjustedCandidates,
            float bonus) where T : ISelectable
        {
            switch (_selectionPivot)
            {
                case SelectionPivot.Mean:
                    adjustedCandidates = MeanNormalizeAdjust(adjustedCandidates.ToList(), bonus, _bonusMode);
                    break;
                case SelectionPivot.Median:
                    adjustedCandidates = MedianNormalizeAdjust(adjustedCandidates.ToList(), bonus, _bonusMode);
                    break;
                default:
                    Debug.LogError(
                        "No default selection method set in Manager. The selector cannot return a selection");
                    break;
            }

            return adjustedCandidates;
        }

        /// <summary>
        ///     Adjusts the selection weights towards the median, and returns the list as an enumeration
        /// </summary>
        private static IEnumerable<T> MedianNormalizeAdjust<T>(List<T> candidates, float bonus, SelectionRange mode)
            where T : class, ISelectable
        {
            IEnumerable<float> weights = candidates.Select(s => s.GetSelectionWeight());
            IOrderedEnumerable<float> sortedWeights = weights.OrderBy(s => s);
            int halfIndex = candidates.Count()/2;
            float median;

            if ((halfIndex%2) == 0)
            {
                median = (sortedWeights.ElementAt(halfIndex) + sortedWeights.ElementAt(halfIndex - 1))/2; 
            }
            else
            {
                median = sortedWeights.ElementAt(halfIndex);
            }

            foreach (T t in candidates)
            {
                t.AdjustedSelectionWeight = AdjustSelectionWeight(t.GetSelectionWeight(), bonus, mode, median);
            }

            return candidates;
        }

        /// <summary>
        ///     Adjusts the selection weights towards the mean, and returns the list as an enumeration
        /// </summary>
        private static IEnumerable<T> MeanNormalizeAdjust<T>(List<T> candidates, float bonus, SelectionRange mode)
            where T : class, ISelectable
        {
            float mean = candidates.Average(s => s.GetSelectionWeight());

            foreach (T t in candidates)
            {
                t.AdjustedSelectionWeight = AdjustSelectionWeight(t.GetSelectionWeight(), bonus, mode, mean);
            }

            return candidates;
        }

        /// <summary>
        ///     Pushes in-range weights towards the target (median or mean) by the bonus value. Results approach the target
        ///     asymptotically, so very high bonus values can be applied without risk of "hiding" common values pushed beyond the
        ///     target. In other words, an infinite bonus would equalize all selection weights.
        /// </summary>
        /// <returns></returns>
        private static float AdjustSelectionWeight(float weight, float bonus, SelectionRange mode, float target)
        {
            float adjustedWeight;

            switch (mode)
            {
                case (SelectionRange.BelowPivot):
                    if (weight < target)
                    {
                        adjustedWeight = weight + (target*(bonus/(bonus + 100)));

                        return adjustedWeight;
                    }
                    return weight;

                case (SelectionRange.AbovePivot):
                    if (weight > target)
                    {
                        adjustedWeight = weight - (target*(bonus/(bonus + 100)));

                        return adjustedWeight;
                    }
                    return weight;

                case (SelectionRange.All):
                    if (weight < target)
                    {
                        adjustedWeight = weight + (target*(bonus/(bonus + 100)));

                        return adjustedWeight;
                    }

                    adjustedWeight = weight - (target*(bonus/(bonus + 100)));

                    return adjustedWeight;
            }

            Debug.LogError("No selection adjustment method selected! Cannot adjust bonus weighting");
            return weight;
        }

        private static T Get<T>(IEnumerable<T> items) where T : ISelectable
        {
            T[] enumerable = items as T[] ?? items.ToArray();
            T[] itemArray = enumerable.ToArray();
            float start = Random.Range(0, enumerable.Sum(i => i.GetSelectionWeight()));
            float[] weights = GetCumulativeWeights(enumerable);

            int index = Array.BinarySearch(weights as Array, start);

            if (index < 0)
                index = (index*-1) - 1;

            return itemArray[index];
        }

        private T[] Get<T>(IEnumerable<T> items, int count) where T : ISelectable
        {
            var results = new List<T>();
            T[] enumerable = items as T[] ?? items.ToArray();
            do
            {
                results.Add(Get(enumerable));
            } while (results.Count < count);

            return results.ToArray();
        }

        private static float[] GetCumulativeWeights<T>(IEnumerable<T> items) where T : ISelectable
        {
            float runningWeight = 0;
            int index = 0;
            T[] enumerable = items as T[] ?? items.ToArray();
            var results = new float[enumerable.Count() + 1];

            foreach (T i in enumerable)
            {
                runningWeight += i.GetSelectionWeight();
                results[index] = runningWeight;

                index++;
            }

            return results;
        }

        /// <summary>
        ///     To apply bonus sweetening, selections are projected into BonusContainers, so we can operate over structs, etc.
        /// </summary>
        private class BonusContainer<T> : ISelectable where T : ISelectable
        {
            public T ObjectRef { get; private set; }
            public readonly float SelectionWeight;

            public BonusContainer(T objRef, float selectionWeight)
            {
                ObjectRef = objRef;
                SelectionWeight = selectionWeight < 0 ? 0 : selectionWeight;
            }

            public float GetSelectionWeight()
            {
                return SelectionWeight;
            }

            public float AdjustedSelectionWeight { get; set; }
        }
    }
}