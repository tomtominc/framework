﻿
namespace Framework
{
    /// <summary>
    /// Objects must implement this interface to be selectable. Any collection of ISelectable-implementing objects can be selected from as long as the collection implements IEnumerable. 
    /// </summary>
    public interface ISelectable
    {
        /// <summary>
        /// Returns a float that represents an item's selection weight. Higher values will get selected more often than lower values.
        /// </summary>
        float GetSelectionWeight();

        float AdjustedSelectionWeight { get; set; }
    }
}
