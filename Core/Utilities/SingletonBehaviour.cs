﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class SingletonBehaviour<T> : SerializedMonoBehaviour where T : SerializedMonoBehaviour
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType < T >();

            return _instance;
        }
    }
}
