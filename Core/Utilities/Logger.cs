﻿using UnityEngine;
using System.Collections;

namespace Framework
{
	public class Logger : MonoBehaviour 
	{
        public void LogMessage ( string message )
        {
            Debug.Log ( message );
        }

        public void LogWarningMessage ( string message )
        {
            Debug.LogWarning ( message );
        }

        public void LogErrorMessage ( string message )
        {
            Debug.LogError ( message );
        }
	}
}