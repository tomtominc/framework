﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class SceneController : SerializedMonoBehaviour
{
    [SerializeField]
    protected TransitionCamera m_transitionCamera;

    public void LoadScene(string p_scene)
    {
        LoadScene(new TransitionData(p_scene));
    }

    public void LoadScene(TransitionData transitionData)
    {
        StartCoroutine(LoadSceneRoutine(transitionData.scene));
    }

    public void SwitchScenes(string p_scene, 
                             Action p_onPreTransitionIn, Action p_onPostTransitionIn)
    {
        SwitchScenes(new TransitionData(p_scene), p_onPreTransitionIn, p_onPostTransitionIn);
    }

    public void SwitchScenes(TransitionData transitionData, 
                             Action p_onPreTransitionIn, Action p_onPostTransitionIn)
    {
        StartCoroutine(SwitchScenesRoutine(transitionData, p_onPreTransitionIn, p_onPostTransitionIn));
    }

    protected IEnumerator SwitchScenesRoutine(TransitionData transitionData, 
                                              Action p_onPreTransitionIn, Action p_onPostTransitionIn)
    {
        m_transitionCamera.DoTransitionEffect(transitionData.inDuration, 1f);

        yield return new WaitForSeconds(transitionData.inDuration);
        yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
        yield return StartCoroutine(LoadSceneRoutine(transitionData.scene));

        if (null != p_onPreTransitionIn)
            p_onPreTransitionIn();

        yield return new WaitForSeconds(0.1f);

        m_transitionCamera.DoTransitionEffect(transitionData.outDuration, 0f);

        yield return new WaitForSeconds(transitionData.outDuration);

        if (null != p_onPostTransitionIn)
            p_onPostTransitionIn();
    }

   
    protected IEnumerator LoadSceneRoutine(string scene)
    {
        yield return SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
        Scene loadedScene = SceneManager.GetSceneByName(scene);
        SceneManager.SetActiveScene(loadedScene);
    }
}
