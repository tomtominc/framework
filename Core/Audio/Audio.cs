﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioType
{
    Sound,
    Music
}

[System.Serializable]
public class Audio
{
    public string key;
    public AudioType type;
    public bool loop;
    public AudioClip clip;
    [Range(0f, 10f)]
    public float volume;

}
