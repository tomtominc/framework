﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : SingletonBehaviour < AudioManager >
{
    [SerializeField]private bool m_playMusic = true;
    [SerializeField]private bool m_playSound = true;

    [SerializeField]public List < Audio > m_storage;

    [SerializeField]public List < AudioSource > m_musicSources;

    public void Play(AudioKey key, int slot = 0)
    {
        Play(key.value, slot);
    }

    public void Play(int index, int slot = 0)
    {
        if (index < m_storage.Count)
            Play(m_storage[index], slot);
    }

    public void Play(string key, int slot = 0)
    {
        Audio audio = m_storage.Find(x => x.key == key);

        if (audio != null)
        {
            Play(audio, slot);
        }
    }

    public void Play(Audio audio, int slot = 0)
    {
        if (slot >= m_musicSources.Count)
            CreateSources(slot);
        
        switch (audio.type)
        {
            case AudioType.Music:
                PlayMusic(audio, slot);
                break;
            case AudioType.Sound:
                PlaySound(audio, slot);
                break;
        }
    }

    public void StopMusic(int slot = 0)
    {
        if (slot < m_musicSources.Count)
            m_musicSources[slot].Stop();
    }

    private void PlayMusic(Audio audio, int slot = 0)
    {
        if (m_playMusic)
        {
            m_musicSources[slot].clip = audio.clip;
            m_musicSources[slot].volume = audio.volume;
            m_musicSources[slot].loop = audio.loop;
            m_musicSources[slot].Play();
        }
   
    }

    private void PlaySound(Audio audio, int slot = 0)
    {
        if (m_playSound)
            m_musicSources[slot].PlayOneShot(audio.clip, audio.volume);
    }

    private void CreateSources(int slot)
    {
        for (int i = m_musicSources.Count; i <= slot; i++)
        {
            string sourceName = string.Format("Source[{0}]", i);
            GameObject audioSource = new GameObject(sourceName, typeof(AudioSource));
            audioSource.transform.SetParent(transform);
            m_musicSources.Add(audioSource.GetComponent < AudioSource >());
        }
    }
   
}
