﻿using UnityEngine;
using System.Collections;

#if ENABLE_VECTROSITY
using Vectrosity;
#endif

namespace Framework
{
	public static class Raycast 
	{
        public static RaycastHit2D Cursor2D ( int layerMask ) 
        {
            Vector3 mousePosition = Input.mousePosition;
            mousePosition.z = 5f;

            Vector2 origin = Camera.main.ScreenToWorldPoint(mousePosition);
            RaycastHit2D info =  Physics2D.Raycast ( origin, Vector2.zero, Mathf.Infinity, layerMask );

            return info;
        }

        public static RaycastHit Cursor ( int layerMask ) 
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit info;

            Physics.SphereCast ( ray, 0.2f, out info, Mathf.Infinity, layerMask );

            return info;
        }

        public static RaycastHit[] CursorAll ( int layerMask , float radius) 
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            return Physics.SphereCastAll ( ray, radius, Mathf.Infinity, layerMask );
        }


	}
}