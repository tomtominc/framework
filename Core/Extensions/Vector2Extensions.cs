﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Framework;

public static class Vector2Extensions
{
    public static float GetRandom(this Vector2 vector)
    {
        return Random.Range(vector.x, vector.y);
    }

    public static Vector2 Clamp(this Vector2 vector, Vector2 clamp)
    {
        return new Vector2(Mathf.Clamp(vector.x, -clamp.x, clamp.x), Mathf.Clamp(vector.y, -clamp.y, clamp.y));
    }

    public static bool InBetween(this Vector2 vector, int other)
    {
        return (vector.x <= other && vector.y >= other);
    }

    public static bool InBetween(this Vector2 vector, float value)
    {
        return (value >= vector.x && value <= vector.y);
    }

    public static Vector3 Midpoint(this Vector3 vector, Vector3 other)
    {
        return (vector + other) / 2f;
    }

    public static Vector3 AddX(this Vector3 vector, float x)
    {
        return new Vector3(vector.x + x, vector.y, vector.z);
    }

    public static Vector3 AddY(this Vector3 vector, float y)
    {
        return new Vector3(vector.x, vector.y + y, vector.z);
    }

    public static Vector3 AddZ(this Vector3 vector, float z)
    {
        return new Vector3(vector.x, vector.y, vector.z + z);
    }

    public static Vector3 Add(this Vector3 vector, float x, float y, float z)
    {
        return new Vector3(vector.x + x, vector.y + y, vector.z + z);
    }

    public static List<Vector2> Points(this Rect rect)
    {
        List<Vector2> points = new List < Vector2>();

        for (int i = (int)rect.x; i < (int)rect.width; i++)
        {
            for (int j = (int)rect.y; j < (int)rect.height; j++)
            {
                points.Add(new Vector2((float)i, (float)j));
            }
        }

        return points;
      
    }

    public static bool inTriangle(this Vector2 p_pt, Vector2 p_v1, Vector2 p_v2, Vector2 p_v3)
    {
        bool l_b1, l_b2, l_b3;

        l_b1 = sign(p_pt, p_v1, p_v2) < 0f;
        l_b2 = sign(p_pt, p_v2, p_v3) < 0f;
        l_b3 = sign(p_pt, p_v3, p_v1) < 0f;

        return (l_b1 == l_b2) && (l_b2 == l_b3);
    }

    public static float sign(Vector2 p_p1, Vector2 p_p2, Vector2 p_p3)
    {
        return (p_p1.x - p_p3.x) * (p_p2.y - p_p3.y) - (p_p2.x - p_p3.x) * (p_p1.y - p_p3.y);
    }

    public static Vector2 GetRandomPoint(this Rect rect)
    {
        return new Vector2(Random.Range(rect.x, rect.width), Random.Range(rect.y, rect.height));
    }

    public static float Sum(this Vector2 v0)
    {
        return v0.x + v0.y;
    }
}