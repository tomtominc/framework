﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//namespace Framework.Core
//{


public static class GameObjectExtensions
{
    /// <summary>
    /// Finds all objects of type T, this also includes inactive objects
    /// </summary>
    /// <returns>The all objects of type.</returns>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static List < T > FindAllObjectsOfType < T >()
    {
        List < T > list = new List < T >();

        Transform[] activeObjects = GameObject.FindObjectsOfType < Transform >();

        for (int i = 0; i < activeObjects.Length; i++)
        {
            T[] components = activeObjects[i].GetComponentsInChildren < T >(true);

            if (components != null && components.Length > 0)
            {
                list.AddRange(components.ToList());
            }

            list = list.Distinct().ToList();
        }

        return list;
    }

    public static Vector2 ToPoint(this int value, int columnSize)
    {
        return new Vector2(value % columnSize, (float)((int)(value / columnSize)));
    }


    public static List < TInterface > FindAllInterfaces <TInterface>(this Component thisComponent) where TInterface : class
    {
        return thisComponent.transform.root.GetComponentsInChildren < TInterface >(true).ToList();
    }

    public static Bounds OrthographicBounds(this Camera camera)
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = camera.orthographicSize * 2;
        Bounds bounds = new Bounds(
                            camera.transform.position,
                            new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
        return bounds;
    }

    public static bool InBounds(this Camera camera, Vector2 point)
    {
        Bounds bounds = camera.OrthographicBounds();

        return(bounds.min.x <= point.x && bounds.min.y <= point.y && bounds.max.x >= point.x && bounds.max.y >= point.y);
    }


    public static Vector3 CanvasOverlayPoint(this GameObject WorldObject, Camera camera, Canvas canvas)
    {
        //this is the ui element
        //RectTransform UI_Element;

        //first you need the RectTransform component of your canvas
        RectTransform CanvasRect = canvas.GetComponent<RectTransform>();

        //then you calculate the position of the UI element
        //0,0 for the canvas is at the center of the screen, whereas WorldToViewPortPoint treats the lower left corner as 0,0. Because of this, you need to subtract the height / width of the canvas * 0.5 to get the correct position.

        Vector2 ViewportPosition = camera.WorldToViewportPoint(WorldObject.transform.position);

        Vector2 WorldObject_ScreenPosition = new Vector2(
                                                 ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
                                                 ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));

        //now you can set the position of the ui element
        return WorldObject_ScreenPosition;
    }

    public static Vector2 WorldToCanvas(this Canvas canvas,
                                        Vector3 world_position,
                                        Camera camera = null)
    {
        if (camera == null)
        {
            camera = Camera.main;
        }

        var viewport_position = camera.WorldToViewportPoint(world_position);
        var canvas_rect = canvas.GetComponent<RectTransform>();

        return new Vector2((viewport_position.x * canvas_rect.sizeDelta.x) - (canvas_rect.sizeDelta.x * 0.5f),
            (viewport_position.y * canvas_rect.sizeDelta.y) - (canvas_rect.sizeDelta.y * 0.5f));
    }

    public static Vector2 WorldToCanvasSpace(this Canvas canvas, Vector3 worldPosition)
    {
        Vector3 viewport_position = Camera.main.WorldToViewportPoint(worldPosition);
        RectTransform canvas_rect = canvas.GetComponent<RectTransform>();
        return new Vector2((viewport_position.x * canvas_rect.sizeDelta.x) -
            (canvas_rect.sizeDelta.x * 0.5f), (viewport_position.y * canvas_rect.sizeDelta.y) -
            (canvas_rect.sizeDelta.y * 0.5f));
    }

    public static int Move(this int number, int steps, int max)
    {
        int direction = (int)Mathf.Sign(steps);

        for (int i = 0; i < Mathf.Abs(steps); i++)
        {
            number += (1 * direction);

            if (number > max)
            {
                number = 0; 
                direction *= -1;
            }

            if (number < 0)
            {
                number = max;
                direction *= -1;
            }
        }

        return number;
    }

    public static int Wrap(this int n, int steps, int count)
    {
        int direction = (int)Mathf.Sign(steps);
        int absSteps = Mathf.Abs(steps);

        for (int i = 0; i < absSteps; i++)
        {
            n = n + direction;

            if (n < 0)
                n = count - 1;
            if (n >= count)
                n = 0;
        }

        return n;
    }

    public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
    }

    //Draws just the box at where it is currently hitting.
    public static void DrawBoxCastOnHit(Vector3 origin, Vector3 halfExtents, Quaternion orientation, Vector3 direction, float hitInfoDistance, Color color)
    {
        origin = CastCenterOnCollision(origin, direction, hitInfoDistance);
        DrawBox(origin, halfExtents, orientation, color);
    }

    //Draws the full box from start of cast to its end distance. Can also pass in hitInfoDistance instead of full distance
    public static void DrawBoxCastBox(Vector3 origin, Vector3 halfExtents, Quaternion orientation, Vector3 direction, float distance, Color color)
    {
        direction.Normalize();
        Box bottomBox = new Box(origin, halfExtents, orientation);
        Box topBox = new Box(origin + (direction * distance), halfExtents, orientation);

        Debug.DrawLine(bottomBox.backBottomLeft, topBox.backBottomLeft, color);
        Debug.DrawLine(bottomBox.backBottomRight, topBox.backBottomRight, color);
        Debug.DrawLine(bottomBox.backTopLeft, topBox.backTopLeft, color);
        Debug.DrawLine(bottomBox.backTopRight, topBox.backTopRight, color);
        Debug.DrawLine(bottomBox.frontTopLeft, topBox.frontTopLeft, color);
        Debug.DrawLine(bottomBox.frontTopRight, topBox.frontTopRight, color);
        Debug.DrawLine(bottomBox.frontBottomLeft, topBox.frontBottomLeft, color);
        Debug.DrawLine(bottomBox.frontBottomRight, topBox.frontBottomRight, color);

        DrawBox(bottomBox, color);
        DrawBox(topBox, color);
    }

    public static void DrawBox(Vector3 origin, Vector3 halfExtents, Quaternion orientation, Color color)
    {
        DrawBox(new Box(origin, halfExtents, orientation), color);
    }

    public static void DrawBox(Box box, Color color)
    {
        Debug.DrawLine(box.frontTopLeft, box.frontTopRight, color);
        Debug.DrawLine(box.frontTopRight, box.frontBottomRight, color);
        Debug.DrawLine(box.frontBottomRight, box.frontBottomLeft, color);
        Debug.DrawLine(box.frontBottomLeft, box.frontTopLeft, color);

        Debug.DrawLine(box.backTopLeft, box.backTopRight, color);
        Debug.DrawLine(box.backTopRight, box.backBottomRight, color);
        Debug.DrawLine(box.backBottomRight, box.backBottomLeft, color);
        Debug.DrawLine(box.backBottomLeft, box.backTopLeft, color);

        Debug.DrawLine(box.frontTopLeft, box.backTopLeft, color);
        Debug.DrawLine(box.frontTopRight, box.backTopRight, color);
        Debug.DrawLine(box.frontBottomRight, box.backBottomRight, color);
        Debug.DrawLine(box.frontBottomLeft, box.backBottomLeft, color);
    }

    public struct Box
    {
        public Vector3 localFrontTopLeft     { get; private set; }

        public Vector3 localFrontTopRight    { get; private set; }

        public Vector3 localFrontBottomLeft  { get; private set; }

        public Vector3 localFrontBottomRight { get; private set; }

        public Vector3 localBackTopLeft      { get { return -localFrontBottomRight; } }

        public Vector3 localBackTopRight     { get { return -localFrontBottomLeft; } }

        public Vector3 localBackBottomLeft   { get { return -localFrontTopRight; } }

        public Vector3 localBackBottomRight  { get { return -localFrontTopLeft; } }

        public Vector3 frontTopLeft     { get { return localFrontTopLeft + origin; } }

        public Vector3 frontTopRight    { get { return localFrontTopRight + origin; } }

        public Vector3 frontBottomLeft  { get { return localFrontBottomLeft + origin; } }

        public Vector3 frontBottomRight { get { return localFrontBottomRight + origin; } }

        public Vector3 backTopLeft      { get { return localBackTopLeft + origin; } }

        public Vector3 backTopRight     { get { return localBackTopRight + origin; } }

        public Vector3 backBottomLeft   { get { return localBackBottomLeft + origin; } }

        public Vector3 backBottomRight  { get { return localBackBottomRight + origin; } }

        public Vector3 origin { get; private set; }

        public Box(Vector3 origin, Vector3 halfExtents, Quaternion orientation)
            : this(origin, halfExtents)
        {
            Rotate(orientation);
        }

        public Box(Vector3 origin, Vector3 halfExtents)
        {
            this.localFrontTopLeft = new Vector3(-halfExtents.x, halfExtents.y, -halfExtents.z);
            this.localFrontTopRight = new Vector3(halfExtents.x, halfExtents.y, -halfExtents.z);
            this.localFrontBottomLeft = new Vector3(-halfExtents.x, -halfExtents.y, -halfExtents.z);
            this.localFrontBottomRight = new Vector3(halfExtents.x, -halfExtents.y, -halfExtents.z);

            this.origin = origin;
        }


        public void Rotate(Quaternion orientation)
        {
            localFrontTopLeft = RotatePointAroundPivot(localFrontTopLeft, Vector3.zero, orientation);
            localFrontTopRight = RotatePointAroundPivot(localFrontTopRight, Vector3.zero, orientation);
            localFrontBottomLeft = RotatePointAroundPivot(localFrontBottomLeft, Vector3.zero, orientation);
            localFrontBottomRight = RotatePointAroundPivot(localFrontBottomRight, Vector3.zero, orientation);
        }
    }

    //This should work for all cast types
    static Vector3 CastCenterOnCollision(Vector3 origin, Vector3 direction, float hitInfoDistance)
    {
        return origin + (direction.normalized * hitInfoDistance);
    }

    static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion rotation)
    {
        Vector3 direction = point - pivot;
        return pivot + rotation * direction;
    }

}
//}