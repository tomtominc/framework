﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprite/HSVShader" 
{
    Properties 
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)

        [MaterialToggle] PixelSnap ( "Pixel Snap", Float ) = 0

        _HueShift("HueShift", Float ) = 0
        _Sat("Saturation", Float) = 1
        _Val("Value", Float) = 1

        [PerRendererData] _Outline ("Outline", Float) = 0
        [PerRendererData] _OutlineColor ("Outline Color", Color) = (1,1,1,1)

        _CutStrip ("CutStrip", Range(0, 1) ) = 0
        _PixelsPerUnit("Pixels Per Unit", Float)= 16

    }
    SubShader 
    {
 
        Tags 
        { 
        	"Queue"="Transparent" 
        	"IgnoreProjector"="True" 
        	"RenderType" = "Transparent" 
        	"PreviewType"="Plane"
        	"CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
 
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma target 2.0
 
            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _MainTex_TexelSize;
            float4 _Color;
            float _HueShift;
            float _Sat;
            float _Val;
            float _Alpha;
            float _Outline;
            float _CutStrip;
            fixed4 _OutlineColor;
            float _PixelsPerUnit;

			float4 AlignToPixelGrid(float4 vertex)
			{
				float4 worldPos = mul(unity_ObjectToWorld, vertex);

				worldPos.x = floor(worldPos.x * _PixelsPerUnit + 0.5) / _PixelsPerUnit;
				worldPos.y = floor(worldPos.y * _PixelsPerUnit + 0.5) / _PixelsPerUnit;

				return mul(unity_WorldToObject, worldPos);
			}
 
            float3 shift_col(float3 RGB, float3 shift)
            {
                float3 RESULT = float3(RGB);
                float VSU = shift.z*shift.y*cos(shift.x*3.14159265/180);
                float VSW = shift.z*shift.y*sin(shift.x*3.14159265/180);
               
                RESULT.x = (.299*shift.z+.701*VSU+.168*VSW)*RGB.x
                        + (.587*shift.z-.587*VSU+.330*VSW)*RGB.y
                        + (.114*shift.z-.114*VSU-.497*VSW)*RGB.z;
               
                RESULT.y = (.299*shift.z-.299*VSU-.328*VSW)*RGB.x
                        + (.587*shift.z+.413*VSU+.035*VSW)*RGB.y
                        + (.114*shift.z-.114*VSU+.292*VSW)*RGB.z;
               
                RESULT.z = (.299*shift.z-.3*VSU+1.25*VSW)*RGB.x
                        + (.587*shift.z-.588*VSU-1.05*VSW)*RGB.y
                        + (.114*shift.z+.886*VSU-.203*VSW)*RGB.z;
               
            	return (RESULT);
            }
 
            struct v2f 
            {
                float4  pos : SV_POSITION;
                float2  uv : TEXCOORD0;
            };

            v2f vert (appdata_base IN)
            {
                v2f OUT;

                float4 alignedPos = AlignToPixelGrid(IN.vertex);

                OUT.pos = UnityObjectToClipPos (alignedPos);
                OUT.uv = TRANSFORM_TEX(IN.texcoord, _MainTex);

                return OUT;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;
                float3 shift = float3(_HueShift, _Sat, _Val);

                fixed4 c = fixed4( half3(shift_col(col, shift)), col.a);

           	 if ( _Outline > 0 && c.a == 0)
                {
                	fixed4 pixelUp = tex2D( _MainTex, i.uv + fixed2(0,_MainTex_TexelSize.y));
                	fixed4 pixelDown = tex2D( _MainTex, i.uv - fixed2(0,_MainTex_TexelSize.y));
                	fixed4 pixelRight = tex2D( _MainTex, i.uv + fixed2(_MainTex_TexelSize.x,0));
                	fixed4 pixelLeft = tex2D( _MainTex, i.uv - fixed2(_MainTex_TexelSize.x,0));
                		 
                	if ( pixelUp.a != 0 || pixelDown.a != 0  || pixelRight.a != 0  || pixelLeft.a != 0)
                	{
                		c.rgba = fixed4(1,1,1,1) * _OutlineColor;
                	}
                }

                return c;

            }
            ENDCG
        }
    }
    Fallback "Diffuse"
}