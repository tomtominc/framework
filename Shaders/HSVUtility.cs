﻿using UnityEngine;
using System;
using DG.Tweening;
using System.Collections;

/// <summary>
/// A Static class that provides helpers
/// to common 2D sprite coloring. Things like being
/// damaged / powering up / poisoned / and generic 
/// methods so I can create my own custom effects
/// this class only works with the 'HSVSprite' Shader on 
/// the sprite you pass into the helper method.
/// </summary>
public static class HSVUtility
{

    public static void Damage(SpriteRenderer renderer, float duration, Action onComplete)
    {
        string valueProperty = "_Val";

        renderer.material.SetFloat(valueProperty, 10f);

        renderer.material.DOFloat(1f, valueProperty, duration).OnComplete 
        (
            () =>
            {
                if (onComplete != null)
                    onComplete();
            }
        );
    }

    public static IEnumerator Damage(SpriteRenderer renderer, float duration)
    {
        string valueProperty = "_Val";

        renderer.material.SetFloat(valueProperty, 10f);

        Tweener materialTween = renderer.material.DOFloat(1f, valueProperty, duration);

        yield return materialTween.WaitForCompletion();
    }

    public static IEnumerator Exit(SpriteRenderer renderer, float duration)
    {
        string valueProperty = "_Val";
        string colorProperty = "_Color";

        float durationHalved = duration * 0.5f;

        Tweener valueTween = renderer.material.DOFloat(10f, valueProperty, durationHalved);

        yield return valueTween.WaitForCompletion();

        Tweener colorTween = renderer.material.DOFade(0f, colorProperty, durationHalved);

        yield return colorTween.WaitForCompletion();

    }

    public static void GlowLoop(SpriteRenderer renderer)
    {
        renderer.material.SetFloat("_Val", 0.5f);

        renderer.material.DOFloat(1.5f, "_Val", 1f).SetLoops(-1, LoopType.Yoyo);
    }

    public static Sequence DamageWithInvincibility(SpriteRenderer renderer, float damageDuration, float overlayPower,
                                                   int blinkCount, Action onCompleteDamage, Action onCompleteBlink)
    {
        string valueProperty = "_Val";

        renderer.material.SetFloat(valueProperty, overlayPower);

        Sequence sequence = DOTween.Sequence();

        sequence.Append
        (

            renderer.material.DOFloat(1f, valueProperty, damageDuration).OnComplete 
            (
                () =>
                {
                    if (onCompleteDamage != null)
                        onCompleteDamage();
                }
            )
        );

        for (int i = 0; i < blinkCount; i++)
        {
            sequence.Append(renderer.material.DOFade(0f, 0.1f));
            sequence.Append(renderer.material.DOFade(1f, 0.1f));
        }

        sequence.OnComplete(() =>
            {
                if (onCompleteBlink != null)
                    onCompleteBlink();

            });

        sequence.Play(); 

        return sequence;

    }

    public static Sequence DamageWithInvincibility(SpriteRenderer renderer, float damageDuration, 
                                                   int blinkCount, Action onCompleteDamage, Action onCompleteBlink)
    {
        string valueProperty = "_Val";

        renderer.material.SetFloat(valueProperty, 10f);

        Sequence sequence = DOTween.Sequence();

        sequence.Append
        (

            renderer.material.DOFloat(1f, valueProperty, damageDuration).OnComplete 
            (
                () =>
                {
                    if (onCompleteDamage != null)
                        onCompleteDamage();
                }
            )
        );

        for (int i = 0; i < blinkCount; i++)
        {
            sequence.Append(renderer.material.DOFade(0f, 0.1f));
            sequence.Append(renderer.material.DOFade(1f, 0.1f));
        }

        sequence.OnComplete(() =>
            {
                if (onCompleteBlink != null)
                    onCompleteBlink();

            });

        sequence.Play(); 

        return sequence;

    }
}
