﻿using UnityEngine;


[ExecuteInEditMode]
/// <summary>
/// Place this on a camera and set to only see things on
/// some 'Reflection' layer. This will create a RenderTexture
/// of whatever is on that layer and give a texture to all shaders.
/// </summary>
public class ScreenReflection : MonoBehaviour
{
    [HideInInspector] [SerializeField] private Camera _camera;

    /// <summary>
    /// How far do we down res the render texture
    /// </summary>
    [SerializeField] private int _downResFactor = 1;

    /// <summary>
    /// which filter mode to use for the render texture
    /// </summary>
    [SerializeField] private FilterMode filterMode;

    /// <summary>
    /// How many pixels per unit do we use
    /// </summary>
    [SerializeField] private int pixelsPerUnit = 128;

    /// <summary>
    /// How visible is the refraction?
    /// </summary>
    [SerializeField] [Range(0, 1)] private float _refractionVisibility = 0;

    [SerializeField] [Range(0, 0.1f)] private float _refractionMagnitude = 0;

    private string _globalTextureName = "_GlobalRefractionTex";
    private string _globalVisibilityName = "_GlobalVisibility";
    private string _globalMagnitudeName = "_GlobalRefractionMag";

    public void VisibilityChange(float value)
    {
        _refractionVisibility = value;
        Shader.SetGlobalFloat(_globalVisibilityName, _refractionVisibility);
    }

    public void MagnitudeChange(float value)
    {
        _refractionMagnitude = value;
        Shader.SetGlobalFloat(_globalMagnitudeName, _refractionMagnitude);
    }

    private void OnEnable()
    {
        GenerateRenderTexture();
        Shader.SetGlobalFloat(_globalVisibilityName, _refractionVisibility);
        Shader.SetGlobalFloat(_globalMagnitudeName, _refractionMagnitude);
    }

    /// <summary>
    /// Raises the validate event.
    /// </summary>
    private void OnValidate()
    {
        Shader.SetGlobalFloat(_globalVisibilityName, _refractionVisibility);
        Shader.SetGlobalFloat(_globalMagnitudeName, _refractionMagnitude);
    }

    /// <summary>
    /// Method that generates the Render Texture.
    /// </summary>
    private void GenerateRenderTexture()
    {
        // Grab the camera component if it's null
        if (_camera == null)
            _camera = GetComponent<Camera>();

        // check if the target texture is null, if so, destroy it.
        if (_camera.targetTexture != null)
        {
            RenderTexture temp = _camera.targetTexture;

            _camera.targetTexture = null;
            DestroyImmediate(temp);
        }

        // create a new render texture
        _camera.targetTexture = new RenderTexture(_camera.pixelWidth >> _downResFactor, _camera.pixelHeight >> _downResFactor, pixelsPerUnit);
        // set the filter mode to what is set in the render settings.
        _camera.targetTexture.filterMode = filterMode;

        // set the global texture for all shaders
        Shader.SetGlobalTexture(_globalTextureName, _camera.targetTexture);
    }
}